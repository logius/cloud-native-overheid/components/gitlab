Gitlab
=========

# Sources
* [GitLab release documentation](https://about.gitlab.com/releases/)
* [GitLab chart](https://gitlab.com/gitlab-org/charts/gitlab)
* [GitLab Runner chart](https://gitlab.com/gitlab-org/charts/gitlab-runner)

# Ingress

There are two ingresses for the GitLab webservice: 'external ingress' and 'admin ingress'. The external ingress is for end users and will typically be connected to the global loadbalancer.
The admin ingress is for connecting the GitLab products (e.g. runner to gitlab core) and for the admin user.

# Active and Standby in a multi DC setup.

The Ansible role for GitLab has features for backup and failover between an active and a standby site. The supported architecture is a setup with two sites, where only one site is active, the other site is standby. There are two main scenarios:
- Planned Migration: the active site is put in maintenance mode, the passive site becomes active. In this proces, the data is transfered from active to passive with backups. Note the backup only includes database and git, s3 object storage is not part of the backup. In the planned migration the external ingress is removed to disconnect a site from the loadbalancer. The passsive site is activated by enabling the ingress and restoring the data.
- Disaster Recovery: the active site goes down. The standby site will be promoted to active. The last available backup is restored.

The following tags are provided to support these scenarios:

- site_shutdown: Disable the active site and saveguard the data. Delete external ingress, suspend cronjobs, perform backup, transfer all data to the standby site.  
- site_test_recover: Install GitLab and restore data from the last available backup.
External ingress and Backups are NOT enabled. The resulting installation can be used for testing and can be promoted to the active site by using 'site_recover'.
- site_recover: Make this the active site. Restore latest backup, create external ingress and activate cronjobs for s3 replication and backup.

For a DR recovery plan, there must also be a procedure to make sure there will never be two active sites. This exercise is not handled by the ansible role but must be handled by the admin user or by the Load balancer.

Note that Velero is not used in the backup and recovery, because components in GitLab depend on the admin ingress. A restore with Velero would result in GitLab components refering to the original site and not the restored site.

# Logging in as a non-Keycloak user
The automatic redirect to Keycloak can be avoided: navigate to `<gitlab url>/users/sign_in?auto_sign_in=false`.

# Use images from another location

Use the following settings to configure from which location images are pulled:

| setting | default from chart | description |
| --- | --- | --- |
| config.gitlab.images.runner_repo | gitlab/gitlab-runner |
| config.gitlab.images.runner_tag | alpine-v{VERSION} |
| config.gitlab.images.minio_repo | minio/minio |
| config.gitlab.images.minio_tag | RELEASE.2017-12-28T01-21-00Z |
| config.gitlab.images.miniomc_repo | minio/mc |
| config.gitlab.images.miniomc_tag | latest |

Example:
```yaml
config:
  gitlab:
    images:
      runner_repo: your.docker-registry/docker-hub-proxy/gitlab/gitlab-runner
      runner_tag: alpine-v13.6.0
      minio_repo: your.docker-registry/project1/minio/minio
      # minio_tag: use version from chart
      miniomc_repo: your.docker-registry/project1/minio/mc
      # miniomc_tag: : use version from chart
```
#### Keyhub

To be able to link the gitlab component with OIDC There is an option to setup OIDC for gitlab using Keyhub. Gitlab is by default configured to use keycloak, the default configuration can be found in path __vars/main.yml__.

Configuration:
```
config:
  system:
  gitlab:
    identity_provider: "keycloak"
```
To use Keyhub as IDP you have to set the following required settings in your cluster config:

* `identity_provider: "keyhub"`
* `config.system.identity_provider_url`  

.Note: this will overwrite the default setting in __vars/main.yml__.

Setting of hostmachine:

Make sure the following env are set on the hostmachine to identify with keyhub:

* `KEYHUB_OIDC_GITLAB_CLIENT_ID`  
* `KEYHUB_OIDC_GITLAB_CLIENT_SECRET`

OpenID Connect Scopes:

the following scopes are used by gitlab during authentication to authorize access to a user's details:

```yaml
scope:
  - openid
  - profile
```
