
# Gitlab Architecture and Disaster Recovery

## Introduction

This document provides an overview of the architecture for Gitlab and discusses a scenario for failover to another availability zone.

## Architecture

Gitlab webservice is the main component, supported by services from postgreSQL, sealed secrets and s3. Postgres data is replicated with help from pgBackRest, using the design pattern: https://access.crunchydata.com/documentation/postgres-operator/latest/architecture/disaster-recovery/.

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby)"]
    BackupVaultAZ2(BackupVault)
  end


  subgraph AZ1 ["AZ1 (active)"]

    subgraph Storage
      
      GitlabPluginsVolume(Plugins Volume)
      PostgresVolume(Postgres Volume 2x)
      PostgresBackupVolumeAZ1(Postgres BackupVolume)
    end     
    
    GitlabAZ1(Gitlab 1x)
    PostgresAZ1(Postgres 2x)
    GitlabAZ1 --SQL--> PostgresAZ1
    PostgresAZ1 --> PostgresVolume
    GitlabAZ1 -- objects--> BackupVaultAZ2
    GitlabAZ1 <-- plugins --- GitlabPluginsVolume
    
    PostgresAZ1 --> replication--> BackupVaultAZ2 & PostgresBackupVolumeAZ1
   
  end 
```

### Notes

|        |                                                                                                                                                                   |
| :----- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| HA     | Gitlab team edition runs just one pod, so this give some limitations on HA. The rollover by Gitlab is quite good, users will not quickly notice downtime. |
| Backup | Backups for the database are created by pgBackRest on volume and in S3. Backup of artefacts is out of scope for the Gitlab role.                              |

Ansible role for Gitlab creates sealed secrets for postgres and Gitlab teambot token in s3 bucket. The default installation will use the S3 in the same K8s cluster. For production is prefered to save the sealed secrets in the standby site as shown below. The reason is these secrets should be available to the standby site in case of failover.

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby)"]

    BackupVaultAZ2(BackupVault)
  end

  subgraph DeployerTool ["Platform-runner"]
    AnsibleRole(SonarQube Ansible role) -- sealedsecrets --> BackupVaultAZ2
  end 
```

## Failover

On failover of the datacenter, a restore in the other availability zone can be done quickly, because all the data items (pgBackRest, secrets, Gitlab artefacts) are available in the failover zone.

Promoting the standby is shown in the next diagram. The failover code will read the sealed secrets from the bucket, install these in the Gitlab namespace and proceeds with installing postgres as a standby, promote it and finally install Gitlab.
The standby site has the following preconditions for a failover:

- Postgres operator installed.
- Sealedsecrets controller installed and master keys synchronized with the other AZ.
- An s3-secret in the failover namespace with access credentials to read and write from bucket in S3 in its own AZ.
- PgBackRest backups available in s3 bucket.
- Secrets for Postgres users and teambot token available in s3 bucket.
- Gitlab artefacts available in s3 bucket.

Note that the plugin volume is not replicated. The plugins will only change on new installs of the Gitlab role. 

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby --> active)"]

    subgraph StorageAZ2 [Storage]
      GitlabPluginsVolume(Plugins Volume)
      BackupVaultVolumeAZ2(BackupVault Volume)
      PostgresBackupVolumeAZ2(Postgres BackupVolume)
      PostgresVolume(Postgres Volume 2x)   
    end     

    BackupVaultAZ2(BackupVault)
    BackupVaultAZ2--> BackupVaultVolumeAZ2
    
    GitlabAZ2(Gitlab 1x)
    PostgresAZ2(Postgres 2x)
    GitlabAZ2 --SQL--> PostgresAZ2
    PostgresAZ2 --> PostgresVolume
    GitlabAZ2 -- objects--> BackupVaultAZ2
    GitlabAZ2 <-- plugins --- GitlabPluginsVolume
    PostgresAZ2 --> replication--> BackupVaultAZ2 & PostgresBackupVolumeAZ2
 
  end

  subgraph AZ1 ["AZ1 (down)"]
    down
  end 
```
