# Provision OIDC client secret for Gitlab, if not existing in cluster
- name: Get gitlab secret
  k8s_info:
    api_version: v1
    kind: Secret
    name: "gitlab-oidc"
    namespace: "{{ config.gitlab.namespace }}"
  register: k8s_secret

- name: load existing secret
  set_fact:
    gitlab_oidc_client_secret: "{{ k8s_secret.resources[0].data['oidc_secret'] | b64decode }}"
  when: k8s_secret.resources.0 is defined

- name: generate new Gitlab OIDC client secret
  set_fact:
    gitlab_oidc_client_secret: "{{ 10000000000000000 | random | to_uuid }}"
  when: gitlab_oidc_client_secret is not defined

- name: Set idp relative path
  set_fact:
    idp_relative_path: "{{ config.keycloak.http_relative_path | default('/auth') }}"

- name: Set vars
  set_fact:
    client_id: "{{ config.system.cluster_name }}-gitlab"
    idp_issuer: "https://{{ config.keycloak.external_access.fqdn }}{{ idp_relative_path }}/realms/{{ config.keycloak.realm }}"
    idp_identifier: "{{ config.system.cluster_name }}-gitlab"
    idp_secret: "{{ gitlab_oidc_client_secret }}"
    idp_end_session_endpoint: "https://{{ config.keycloak.external_access.fqdn }}{{ idp_relative_path }}/realms/{{ config.keycloak.realm }}/protocol/openid-connect/logout"

- name: save new secret as K8s secret
  shell: |
    kubectl create secret -n {{ config.gitlab.namespace }} generic gitlab-oidc --from-literal=oidc_secret={{ gitlab_oidc_client_secret }}
  when: k8s_secret.resources.0 is not defined

- name: Set client id
  set_fact:
    client_id: "{{ config.system.cluster_name }}-gitlab"
    client_definition_gitlab:
      clientId: "{{ client_id }}"
      name: "{{ client_id }}"
      secret: "{{ gitlab_oidc_client_secret }}"
      description: "Gitlab client"
      baseUrl: "https://{{ config.gitlab.external_access.fqdn }}"
      redirectUris: ["https://{{ config.gitlab.external_access.fqdn }}/*"]
      directAccessGrantsEnabled: true

  # Username Mapper - the field 'sub' in the JWT will contain the config.gitlab.oidc_claim_sub i.s.o. KeyCloak guid.
  # Can (at least) be used with 'username' and 'email'.
- name: "Configure sub attribute {{ config.gitlab.oidc_claim_sub }}"
  set_fact: 
    client_definition_gitlab: "{{ client_definition_gitlab | combine(mappers) }}"
  vars: 
    mappers: 
      protocolMappers:      
        - config:
            claim.name: "sub"
            access.token.claim: False
            id.token.claim: True
            userinfo.token.claim: True
            user.attribute: "{{ config.gitlab.oidc_claim_sub }}"
            jsonType.label: "String"
            full.path: True         
          name: "{{ config.gitlab.oidc_claim_sub }}"
          consentRequired: false
          protocol: openid-connect
          protocolMapper: "oidc-usermodel-property-mapper"
  when: config.gitlab.oidc_claim_sub is defined

- name: Gitlab OIDC client
  include_role:
    name: common/keycloak
    tasks_from: keycloak_client.yml
  vars:
    clientId: "{{ client_id }}"
    client_definition: "{{ client_definition_gitlab }}"