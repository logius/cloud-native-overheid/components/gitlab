#
# The role supports two installation methods for Postgres:  
# - Bitnami - (default) simple one node install 
# - Crunchy - HA cluster with 3 nodes and pgbackrest
# 
- name: Install postgresql with Bitnami
  block: 
    - name: Install postgres 
      include_role:
        name: common/postgresql-bitnami-v9
        tasks_from: postgres.yml
      vars:
        - database_namespace: "{{ config.gitlab.namespace }}"
        - database: gitlab
        - postgresql_chart_version: "{{ charts.postgresql.version }}"
        - storageClass: "{{ config.gitlab.postgres.storageClass | default() }}"
        - size: "{{ config.gitlab.postgres.size | default(omit) }}"
        - volume_backups: false

    - name: facts for helm values 
      set_fact: 
        postgres_db: "gitlab"
        postgres_user: "postgres"
        postgres_host: "postgresql-11.{{ config.gitlab.namespace }}.svc.cluster.local"
        postgres_secret: postgresql-user

  when: config.gitlab.postgres.installer == 'bitnami'

- name: Install postgresql with Crunchy
  block: 
    - name: Copy secret from backup-vault (using the vault from the same k8s cluster) 
      k8s_copy_secret:
        name: "{{ config.gitlab.minio.secret | default('minio-secret') }}"
        namespace: "{{ config.gitlab.minio.namespace | default(config.backup_vault.namespace) }}"
        target_name: "s3-secret"
        target_namespace: "{{ config.gitlab.namespace }}"

    - name: Create DB using PGO
      include_role:
        name: postgres-operator-v5/cluster
        tasks_from: deploy_cluster.yml 
      vars: 
        - s3_bucket: "{{ platform.prefix }}-gitlab-db{{ s3_bucket_suffix }}"
        - s3_endpoint: "{{ config.gitlab.s3.fqdn }}"
        - database: gitlab-db
        - database_namespace: "{{ config.gitlab.namespace }}"
        - pg_resources: "{{ resources_config.pg_resources | default(omit)}}"
        - storage_size: "{{ config.gitlab.postgres.database.size }}"
        - superuser: true
        - app_specific_psql_parameters:
            max_connections: "{{ config.gitlab.postgres.max_connections | default(200)}}"
        - app_specific_pg_hba:  
            - host all gitlab-db 0.0.0.0/0 md5
        - image_registry: "{{ platform.harbor_registry }}/crunchydata-proxy" 

    - name: facts for helm values 
      set_fact: 
        postgres_db: gitlab-db
        postgres_user: gitlab-db
        postgres_host: gitlab-db-ha
        postgres_secret: gitlab-db-pguser-gitlab-db

  when: config.gitlab.postgres.installer == 'crunchy'

- name: Use existing postgresql database
  set_fact:
    postgres_db: "{{ config.gitlab.postgres_db }}"
    postgres_user: "{{ config.gitlab.postgres_user | default('postgres') }}"
    postgres_host: "{{ config.gitlab.postgres_host }}"
    postgres_secret: "{{ config.gitlab.postgres_secret }}"
  when: config.gitlab.postgres.installer == 'none'
