#!/bin/bash
set -e # stop on error 
set -x # verbose commands 

rails_dir=/srv/gitlab
tmp_path=$rails_dir/tmp
backups_path=$rails_dir/tmp/backups
output_path=$backups_path/0_gitlab_backup.tar
s3config=/etc/gitlab/.s3cfg

function fetch_remote_backup(){
  rm -rf $tmp_path/* 
  mkdir -p $backups_path
  
  s3cmd --config=$s3config get "s3://$BACKUP_BUCKET_NAME/$BACKUP_TIMESTAMP" $output_path
}

function unpack_backup(){
  local file_path=$1
  cd $(dirname $file_path)

  echo "Unpacking backup"

  if [ ! -f $file_path ]; then
    echo $file_path not found
    exit 1
  fi

  date
  tar -xf $file_path
  date
}

function restore(){
  # Cleanup if requested 
  if [ -n "$FORCE_DOWNLOAD" ]; then
    rm -rf $tmp_path/* 
  fi 

  # Skip if the backup is already available 
  if [ ! -f $output_path ]; then
    fetch_remote_backup
  fi 

  # Skip if the previous tar was OK
  if [ ! -f ${output_path}.tar-ok ]; then
    unpack_backup $output_path
    touch ${output_path}.tar-ok
  fi 

  export BACKUP=0 # output_path is fixed to 0

  if [ -n "$RESTORE_DATABASE" ]; then
    gitlab-rake gitlab:db:drop_tables
    gitlab-rake gitlab:backup:db:restore
  fi

  if [ -n "$RESTORE_GIT" ]; then
    gitlab-rake gitlab:backup:repo:restore
  fi

  gitlab-rake cache:clear
}

# main 
restore
