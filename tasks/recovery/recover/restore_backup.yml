#
# Restore backup with a k8s pod. 
#

- name: Check peer cluster config 
  assert: 
    that: config.system.peer_cluster is defined and config.system.peer_cluster | length > 0 
    fail_msg: "config.system.peer_cluster should be defined"

- name: Get DB Pod 
  k8s_info:
    kind: Pod
    namespace: "{{ config.gitlab.namespace }}"
    label_selectors:
      - postgres-operator.crunchydata.com/cluster=gitlab-db
      - postgres-operator.crunchydata.com/role=master
  register: gitlab_db_pods
  failed_when: gitlab_db_pods.resources | length == 0 
  when: restore_database is defined 

  # Due to an issue with GitLab restore, we need to drop pg_stat_statements
  # https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2469
- name: Drop extension pg_stat_statements
  kubernetes.core.k8s_exec:
    namespace: "{{ config.gitlab.namespace }}"
    pod: "{{ gitlab_db_pods.resources[0].metadata.name }}"
    container: database
    command: bash -c "psql -d gitlab-db -c 'drop extension pg_stat_statements'"
  failed_when: false 
  when: restore_database is defined 

  # The gitaly pod fails the livenessprobe when the restore git is progressing for some time. The pod gets killed. 
  # Workaround is to the make the probe less strict. Note the helm chart does not allow for changing the probe. 
- name: Configure liveness probe in gitaly
  kubernetes.core.k8s_json_patch:
    kind: StatefulSet
    namespace: l12m-gitlab
    name: gitlab-gitaly
    patch:
      - op: replace
        path: /spec/template/spec/containers/0/livenessProbe/failureThreshold
        value: 10
      - op: replace
        path: /spec/template/spec/containers/0/livenessProbe/timeoutSeconds
        value: 10
  when: restore_git is defined 

- name: Get Minio Secret
  k8s_secret_info:
    namespace: "{{ config.backup_vault.namespace }}"
    name: minio-secret
  register: minio_secret

- name: Set Backup bucket to peer cluster 
  set_fact: 
    backup_bucket: "{{ config.gitlab.backup.bucket_name }}-{{ config.system.peer_cluster }}"

- name: Download backup bucket
  debug: 
    msg: "Backup bucket: {{ backup_bucket }}"

- name: List Backup tars from peer cluster
  aws_s3:
    aws_access_key: "{{ minio_secret['accesskey'] }}"
    aws_secret_key: "{{ minio_secret['secretkey'] }}"
    bucket: "{{ backup_bucket }}"
    mode: list
    s3_url: "https://{{ config.backup_vault.fqdn }}"
  register: backups

- name: Set latest Backup
  set_fact:
    backup_name: "{{ backups.s3_keys | select('match', '[^secrets]') | sort | last | replace('_gitlab_backup.tar', '') }}"

- name: Fetch Cronjob for backup, and use it as a template for our Pod 
  k8s_info:
    name: gitlab-toolbox-backup
    namespace: "{{ config.gitlab.namespace }}"
    api_version: batch/v1beta1 # should be v1 but GitLab is still using v1beta1
    kind: CronJob
  register: backup_cronjob 

- name: restore-script secret
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: restore-script
        namespace: "{{ config.gitlab.namespace }}"
      type: Opaque
      stringData:
        restore.sh: "{{ lookup('file', 'restore_gitlab.sh') }}"

- name: Delete existing restore pod
  k8s:
    state: absent
    api_version: v1
    kind: Pod
    name: "{{ podname }}"
    namespace: "{{ config.gitlab.namespace }}"    
    wait: true 
    
- name: Create restore pod. 
  k8s:
    state: present
    force: true
    definition:
      apiVersion: v1
      kind: Pod
      metadata:
        name: "{{ podname }}"
        namespace: "{{ config.gitlab.namespace }}"
      spec: 
        containers:
        - name: toolbox-restore
          image: "{{ container.image }}"
          args: [ "bash", "-c", "/restore/bin/restore.sh" ]
          env: "{{ container.env + extra_env }}"
          resources: "{{ container.resources }}" 
          volumeMounts: "{{ container.volumeMounts + extra_volume_mounts }}"
        initContainers: "{{ spec.initContainers }}"
        restartPolicy: Never
        securityContext: "{{ spec.securityContext }}"
        volumes: "{{ spec.volumes + extra_volumes }}"
  vars: 
    spec: "{{ backup_cronjob.resources[0].spec.jobTemplate.spec.template.spec }}"
    container: "{{ backup_cronjob.resources[0].spec.jobTemplate.spec.template.spec.containers[0] }}"
    extra_env:
    - name: BACKUP_BUCKET_NAME
      value: "{{ backup_bucket }}" 
    - name: BACKUP_TIMESTAMP # name of backup in S3 
      value: "{{ backup_name }}"
    - name: RESTORE_GIT # Perform git restore is var is not empty
      value: "{{ restore_git | default ('') }}"
    - name: RESTORE_DATABASE # Perform  database if var is not empty
      value: "{{ restore_database | default ('') }}"
    - name: FORCE_DOWNLOAD # Force download if var is not empty
      value: "{{ force_download | default ('') }}"
    - name: GITLAB_ASSUME_YES
      value: "1"
    extra_volume_mounts:
    - mountPath: /restore/bin
      name: restore-script
    extra_volumes: 
    - name: restore-script
      secret:
        defaultMode: 0777
        secretName: restore-script

- name: Check progress restore
  debug: 
    msg: "Check progress of restore in pod {{ platform.prefix}}-restore in namespace {{ config.gitlab.namespace }}" 

