#
# Helm values for GitLab
#
# Minimal spec: this file only contains parameters where the default is not sufficient.
#
global:
  initialRootPassword: 
    secret: gitlab-gitlab-initial-root-password
    key: password
  kas: 
    enabled: false
  railsSecrets:
    secret: gitlab-rails-secret
  ingress:
    enabled: false
    configureCertmanager: false
    class: {{ config.gitlab.ingress_class }}
  hosts:
    externalIP: {{ config.gitlab.external_ip }}
    domain: {{ config.gitlab.domain }}

    gitlab:
      name: {{ config.gitlab.external_access.fqdn }}

    # Only used if minio is enabled under global.minio.enabled
    minio:
     name: {{ config.gitlab.minio_url | default("gitlab-minio." + config.gitlab.domain) }}
     https: true
  
  appConfig:
    backups:
      bucket: {{ config.gitlab.backup_name_prefix }}-gitlab-backup{{ s3_bucket_suffix }}
      tmpBucket: {{ config.gitlab.backup_name_prefix }}-gitlab-recovery-tmp{{ s3_bucket_suffix }}
    cron_jobs:
      # Use config.gitlab.pipeline_schedule_worker with a cron expression to allow
      # scheduled pipelines to run more often than only once an hour.
      # See https://docs.gitlab.com/charts/charts/globals.html#cron-jobs-related-settings
      pipeline_schedule_worker:
        cron: "{{ config.gitlab.pipeline_schedule_worker | default("19 * * * *") }}"
    defaultProjectsFeatures:
      containerRegistry: {{ config.gitlab.registry.enabled }}
    packages:
      enabled: {{ config.gitlab.packages.enabled }}
    enableUsagePing: false
    usernameChangingEnabled: false
    omniauth:
      enabled: true
      autoSignInWithProvider: "{{ config.gitlab.identity_provider }}"
      syncProfileFromProvider: ["{{ config.gitlab.identity_provider }}"]
      syncProfileAttributes: ['name', 'email']
      allowSingleSignOn: ["{{ config.gitlab.identity_provider }}"]
      blockAutoCreatedUsers: false
      autoLinkLdapUser: false
      autoLinkSamlUser: false
      externalProviders: ["{{ config.gitlab.identity_provider }}"]
      providers:
      - secret: oidc-provider
        key: provider
    terraformState:
      enabled: true
    time_zone: {{ config.gitlab.time_zone }}

  registry:
    enabled: {{ config.gitlab.registry.enabled }}

  psql:
    host: "{{ postgres_host }}"
    database: "{{ postgres_db }}"
    port: 5432
    username: "{{ postgres_user }}" # gitlab requires a super user in order to install database extensions 
    password:
      secret: "{{ postgres_secret }}"
      key: password

  minio:
    enabled: {{ config.gitlab.build_minio | default(true) }}

  certificates:
    image:
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/certificates

{% if config.gitlab.smtp is defined %}
  smtp:
    {{ config.gitlab.smtp | to_nice_yaml | indent }}
{% endif %}

  email:
    from: {{ config.gitlab.email_from | default() }}
    reply_to: {{ config.gitlab.email_reply_to | default() }}

  kubectl:
    image:
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/kubectl

  gitlabBase:
    image:
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-base
      
certmanager:
  install: false

nginx-ingress:
  enabled: false

gitlab-runner:
  install: false

postgresql:
  install: false
  volumePermissions:
    securityContext:
      runAsUser: 1001
  # postgresql.metrics.enabled=false because of:
  # https://gitlab.com/gitlab-org/charts/gitlab/issues/666
  metrics:
    enabled: false

prometheus:
  install: false

minio:
  persistence:
    enabled: {{ config.gitlab.persistence.enabled | default(true) }}
    storageClass: {{ config.gitlab.storageClass | default() }}

{% if config.gitlab.minio_tls_secret is defined %}
  ingress:
    annotations:
      kubernetes.io/ingress.class: nginx
    enabled: {{ config.gitlab.ingress.enabled | default(true) }}
    tls:
      secretName: {{ config.gitlab.minio_tls_secret }}
{% endif %}
  image: {{ config.gitlab.images.minio_repo }}
  imageTag: {{ config.gitlab.images.minio_tag }}

  minioMc:
    image: {{ config.gitlab.images.miniomc_repo }}
    tag: {{ config.gitlab.images.miniomc_tag }}

registry:
  enabled: {{ config.gitlab.registry.enabled }}
  image: 
    repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-container-registry
  ingress:
    enabled: true
    annotations:
      cert-manager.io/cluster-issuer: {{ config.certmanager.issuer | default('none') }}
    tls:
      secretName: {{ config.gitlab.registry_tls_secret | default('gitlab-tls-wildcard') }}
  deployment:
    livenessProbe:
      timeoutSeconds: {{ config.gitlab.registry_livenessProbe_timeout | default(5) }}

redis:
  global:
    imageRegistry: {{ config.gitlab.images.redis_repo }}
  master:
    persistence:
      enabled: {{ config.gitlab.persistence.enabled | default(true) }}
      storageClass: {{ config.gitlab.redis.storageClass | default(config.gitlab.storageClass) | default() }}

shared-secrets:
  selfsign:
    image:
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/cfssl-self-sign

gitlab:
  gitaly:
    extraEnv: {{ config.gitlab.gitaly.extraEnv | default({}) }}
    
    image: 
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitaly

    persistence:
      enabled: {{ config.gitlab.persistence.enabled | default(true) }}
      accessMode: ReadWriteOnce
      storageClass: {{ config.gitlab.gitaly.storageClass | default() }}
  
  gitlab-exporter:
    image:
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-exporter

  gitlab-shell:
    image: 
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-shell

  migrations:
    image:
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-toolbox-ee
    backoffLimit: 2

  toolbox:
    image: 
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-toolbox-ee
    backups:
      objectStorage:
        config:
          key: config
          secret: "{{ config.gitlab.gitlab_minio_s3cfg_secret | default(config.gitlab.gitlab_backup_s3cfg_secret) }}"
      cron:
        enabled: true
        schedule: "{{ config.gitlab.backup_schedule | default('0 1 * * *') }}"
        extraArgs: "{{ (config.gitlab.backup.excludes |default('') + " " + config.gitlab.backup.maximum_backups |default(false) |ternary('--maximum-backups '+ config.gitlab.backup.maximum_backups |default('')|string,''))|trim }}"

        persistence:
          size: "{{ config.gitlab.backup.tmp.volume_size | default('30Gi') }}"
{% if config.gitlab.backup.resources is defined %}
        resources:
          limits:
            cpu: {{ config.gitlab.backup.resources.cpuLimit }}
            memory: {{ config.gitlab.backup.resources.memoryLimit }}
          requests:
            cpu: {{ config.gitlab.backup.resources.cpuRequests }}
            memory: {{ config.gitlab.backup.resources.memoryRequests }}
{% endif %}
    registry:
      enabled: {{ config.gitlab.registry.enabled }}

{% if config.gitlab.backup_pghost is defined %}
    psql:
      host: {{ config.gitlab.backup_pghost }}
{% endif %}        

  sidekiq:
    image: 
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-sidekiq-ee
{% if config.gitlab.forward_proxy.enabled is defined %}
    extraEnv:
      http_proxy: {{ cloudinfra.internet_proxy.HTTP_PROXY }}
      https_proxy: {{ cloudinfra.internet_proxy.HTTPS_PROXY }}
      HTTP_PROXY: {{ cloudinfra.internet_proxy.HTTP_PROXY }}
      HTTPS_PROXY: {{ cloudinfra.internet_proxy.HTTPS_PROXY }}
      NO_PROXY: {{ cloudinfra.internet_proxy.NO_PROXY }}
      no_proxy: {{ cloudinfra.internet_proxy.NO_PROXY }}
{% endif %}
    registry:
      enabled: {{ config.gitlab.registry.enabled }}

  webservice:
    image: 
      repository: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-webservice-ee
{% if config.gitlab.forward_proxy.enabled is defined %}
    extraEnv:
      http_proxy: {{ cloudinfra.internet_proxy.HTTP_PROXY }}
      https_proxy: {{ cloudinfra.internet_proxy.HTTPS_PROXY }}
      HTTP_PROXY: {{ cloudinfra.internet_proxy.HTTP_PROXY }}
      HTTPS_PROXY: {{ cloudinfra.internet_proxy.HTTPS_PROXY }}
      NO_PROXY: {{ cloudinfra.internet_proxy.NO_PROXY }}
      no_proxy: {{ cloudinfra.internet_proxy.NO_PROXY }}
{% endif %}
    registry:
      enabled: {{ config.gitlab.registry.enabled }}

    ingress:
      enabled: false

    workhorse:
      image: {{ config.gitlab.images.gitlab_product_registry }}/gitlab-org/build/cng/gitlab-workhorse-ee